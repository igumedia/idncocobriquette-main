---
title: Gallery
postTitle: Sneak peek of what we do
description: A little of glance at what we do. Our products are ready to ship.
hero: "/uploads/hero-4.jpg"
videoThumbnail: "/uploads/2-thumb.jpeg"
videoURL: https://www.youtube.com/watch?v=GBoF2d4YQdc
type: gallery
listSidebar:
  listTitle: Why choose us ?
  listContent:
  - Worldwide delivery
  - Free samples
  - Listen to your requirement
  - Private-label packaging
gallery:
- descriptionTitle: Kaloud Charcoal
  descriptionText: This is our kaloud shaped charcoal. We also able to make cylinder,
    cubes and hexagonal.
  image: "/uploads/1.jpeg"
- descriptionTitle: Process of the making
  descriptionText: The process of making our charcoal is semi-automatic. At some stage
    we use machines at other stages we use manual labour.
  image: "/uploads/2.jpeg"
- descriptionTitle: Process of the making
  descriptionText: The process of making our charcoal is semi-automatic. At some stage
    we use machines at other stages we use human power
  image: "/uploads/3.jpeg"
- descriptionTitle: Part of family
  descriptionText: Our lovely employees work to cut half-processed charcoal.
  image: "/uploads/4.jpeg"
- descriptionTitle: Custom Packaging
  descriptionText: You can use your own private label. We will print and pack according
    to your requirements.
  image: "/uploads/5.jpeg"
- descriptionTitle: Fully Stuffed Container
  descriptionText: Our previous shipment, fully stuffed container <3
  image: "/uploads/6.jpeg"
- descriptionTitle: Less Ash or Whitish Ash?
  descriptionText: We can provide as per your requirement, we can make it less ash
    or whitish ash
  image: "/uploads/7.jpeg"
- descriptionTitle: A little bit of zoom
  descriptionText: A glance of our cubes charcoal in action.
  image: "/uploads/8.jpeg"
- descriptionTitle: Hexagoal Charcoals
  descriptionText: We can make hexagonal charcoal in various sizes. If you want samples
    you can get it for free! Just leave us a message.
  image: "/uploads/9.jpeg"
- descriptionTitle: Nice beautiful red glowing color
  descriptionText: Nice looking red color when fully burned, visually no smokes, no
    spark.
  image: "/uploads/10.jpeg"
- descriptionTitle: Various shape
  descriptionText: We also able to make cylinder, cubes and hexagonal.
  image: "/uploads/11.jpeg"
- descriptionTitle: Kaloud Charcoal
  descriptionText: This is our kaloud shaped charcoal. We also able to make cylinder,
    cubes and hexagonal.
  image: "/uploads/12.jpeg"
menu:
  nav:
    weight: 3

---
