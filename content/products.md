---
title: Our products
postTitle: All Kind of Coco Briquettes
description: We provide various kinds of charcoal with various specifications. We
  can provide charcoal with shapes such as cubes, cylinders, hexagonal and kaloud.
  If you need in special requirements we will try to deliver it your hand.
hero: "/uploads/hero-2.jpg"
type: single
listSidebar:
  listTitle: 'Our Charcoal Characters :'
  listContent:
  - Less ash
  - Tasteless
  - Neutral odors
  - Virtually no smoke
  - Nice red glowing when fully burned
menu:
  nav:
    name: Products
    weight: 2

---
We provide various kinds of charcoal with various specifications. We can provide charcoal with shapes such as cubes, cylinders, hexagonal and kaloud. If you need in special size we will try to deliver it your hand.

Basically our charcoal are made of pure coconut shell. But if you have special specifications such as wanting whitish ash then we can mix wood as a raw material to make it happen. Occasionally we use a scale of 90% - 10% to get whitish ash. If you have your own scale standard then we will add it into the mix,

In general our charcoal characters are ;

* Less ash
* Virtually no smoke
* Neutral odors
* Tasteless
* Nice red glowing when fully burned

If you have special needs tell us what are your requirements, we will try to make it happen and give you the sample, we will receive your feedback for adjustment if needed.

We don't mind if you want to pack the charcoals with your own brand. We will gladly pack it according to your request. You can make your own design or we can make the design according to your specifications, then we will print and pack as per your requirements.

## Further Information

For further discussion please reach at us :

* E-mail : dviradiyah@yahoo.com
* Phone & WhatsApp : +62 821-3323-9099
* Address : Margoyoso, Pati, Jawa Tengah, Indonesia