---
title: Indonesian Coconut Shell Charcoal
postTitle: Indonesian Coconut Shell Charcoal
description: We provide high quality coconut shell charcoal briquette from Indonesian.
  Our charcoal are good fit for hookah, shisha, incense, BBQ, water pipe, or any other
  burning usage
hero: "/uploads/hero-1.jpg"
type: single
listSidebar:
  listTitle: Why choose us ?
  listContent:
  - Free samples
  - Custom packaging
  - Custom charcoal shapes and sizes
  - Adjust according to your feedback
menu:
  nav:
    name: Home
    weight: 1

---
We are a trading company and supplier specializing in manufacturing coconut charcoal. Our company is based in the city of Semarang, Indonesia. Indonesia is one of the largest coconut producers in the world, so our products use coconut raw materials obtained from farmers, so no trees are cut down in the process.

> We have exporting our products to various countries such as Middle East and Europe. We hope to further develop our market throughout the world. We ship our products using 40 HC and 20 HC containers.

![](/uploads/New Project.jpg)

## Our Charcoal Features

Our charcoals are good fit for hookah, shisha, incense, BBQ, water pipe, or any other burning usage. It’s because our charcoal are :

* Less ash
* Neutral odors
* No spark
* Virtually no smoke
* Good red glowing color when fully burned
* Tasteless

![](/uploads/New Project (2).jpg)

## Our Services

1. **We provide free samples**

    We gladly provide you free samples but we expect customer to pay the shipping cost.

2. **Custom packaging**

    We are able to package according to your needs, we will print your own design and pack based on your specifications.

3. **We able produce charcoal in custom shapes and sizes**

    We will produce according to your needs, either it's cube, hexagonal, cylinder or kaloud. We will do our best to fulfill your needs.
    
4. **We gladly adjust our product according you feedback upon the sample you received.**

    Your feedback are valuable for us. We will improve or adjust our products based on your requirement.

![](/uploads/2.jpeg)

## Price and Further Information

If you need quotation and more specific information please dont hesitate to reach us. We would be very happy to hear from you. You can reach us at :

* E-mail : dviradiyah@yahoo.com
* Phone & WhatsApp : +62 821-3323-9099
* Address : Margoyoso, Pati, Jawa Tengah, Indonesia